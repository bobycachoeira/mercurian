/**
 * Created by boby on 19/09/15.
 */

'use strict';
var veiculoAlterar;


angular.module('mercurianBobyApp')
    .controller('VeiculoController', function GetAllVeiculo ($scope, $window, $http, $mdDialog, $mdToast, $timeout, $location) {
        var last = {
            bottom: false,
            top: true,
            left: false,
            right: true
        };

        $http.defaults.useXDomain = true;

        $http.get(servidor+'/cliente').
            success(function (data) {
                $scope.clientes = data;
            });


        $http.get(servidor + '/veiculo').
            success(function (data) {
                $scope.veiculos = data;
            });


        $scope.edit = function (veiculo) {
            $scope.veiculo = veiculo;
            console.log(veiculo);
            veiculoAlterar = veiculo;
        };

        $scope.novo = function () {
            veiculoAlterar = reset();
        };

        //Essa será executada no click do botão edit ("putUser")
        $scope.veiculo = veiculoAlterar;




        $scope.alteraVeiculo = function (veiculo) {
            $scope.mostraProgressBar = true;
            $http.put(servidor + '/veiculo/alterar', $scope.veiculo).success(function () {
                $scope.veiculos.unshift();
                $scope.mostraProgressBar = false;
                $scope.messageFinalAltera(veiculo,true);
                $location.path('/veiculo');
            }).error(function () {
                $scope.mostraProgressBar = false;
                $scope.messageFinalAltera(veiculo, false);
                $location.path('/veiculo/alterar');
            });
        };


        //Essa será executada no click do botão Deletar ("deleteVeiculo")
        $scope.deleteVeiculo = function (veiculo, event) {

            var url = servidor + '/veiculo/excluir/' + veiculo.id;
            $http.delete(url).success(function () {
                $scope.messageFinalDelete(veiculo, true);
                $location.path('/veiculo/');
            }).error(function () {
                $scope.messageFinalDelete(veiculo, false);
                $location.path('/veiculo/');
            });

        };


        //Essa sera executada quando for clicado o botao Salvar o Veiculo
        $scope.salvaVeiculo = function (veiculo) {
            $scope.mostraProgressBar = true;
            $http.post(servidor + '/veiculo/adicionar', $scope.veiculo).success(function (data) {
                $scope.veiculos.unshift(data);
                $scope.mostraProgressBar = false;
                $scope.messageFinalSalva(veiculo, true);
                $location.path('/veiculo');
            }).error(function (data) {
                $scope.mostraProgressBar = false;
                $scope.messageFinalSalva(veiculo, false);
                $location.path('/veiculo/incluir');
            });
        };

        //Aqui é a funcao para dar refresh na tela
        /*var refresh = function () {
         $window.location.reload();
         };*/

        var reset = function () {
            $scope.veiculo = {
                "id": 0,
                "marca": "",
                "modelo": "",
                "ano": 0,
                "placa": "",
                "cliente": {
                    "id": 0,
                    "nome": "",
                    "email": "",
                    "telefone": "",
                    "telefone2": "",
                    "cidade": {
                        "id": 0,
                        "nome": "",
                        "estado": {
                            "id": 0,
                            "nome": "",
                            "uf": ""
                        }
                    }
                }
            };
            veiculoAlterar = {
                "id": 0,
                "marca": "",
                "modelo": "",
                "ano": 0,
                "placa": "",
                "cliente": {
                    "id": 0,
                    "nome": "",
                    "email": "",
                    "telefone": "",
                    "telefone2": "",
                    "cidade": {
                        "id": 0,
                        "nome": "",
                        "estado": {
                            "id": 0,
                            "nome": "",
                            "uf": ""
                        }
                    }
                }
            }
        };

        /*Mensagem de confirma delecao*/
        $scope.ConfirmaDelecao = function(veiculo, ev) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Deletar o Veiculo?')
                .content('Tem certeza que deseja deletar o Veiculo '+veiculo.marca +' | '+veiculo.modelo +' | do cliente: '+ veiculo.cliente.nome)
                .ariaLabel('Delecao Veiculo')
                .targetEvent(ev)
                .ok('Sim')
                .cancel('Não');
            $mdDialog.show(confirm).then(function() {
                console.log('Deleta');
                $scope.deleteVeiculo(veiculo,ev);
            }, function() {
                console.log('Não Deleta');
            });
        };

        /*Toast sera mostrado ao tentar deletar um Veiculo*/
        $scope.messageFinalDelete = function(veiculo, deletou) {
            if(deletou){
                $mdToast.show(
                    $mdToast.simple()
                        .content('Veiculo '+veiculo.marca+' '+veiculo.modelo+ ' do cliente: '+veiculo.cliente.nome+' deletado com sucesso')
                        .position($scope.getToastPosition())
                        .hideDelay(3000)
                );
            } else{
                $mdToast.show(
                    $mdToast.simple()
                        .content('Problema ao deletar o Veiculo '+veiculo.marca+' '+veiculo.modelo+ ' tente novamente se desejar')
                        .position($scope.getToastPosition())
                        .hideDelay(4000)
                );
            }

        };

        /*Toast do Tipo de Veiculo salvo*/
        $scope.messageFinalSalva = function(veiculo, t) {
            if(t){
                $mdToast.show(
                    $mdToast.simple()
                        .content('Veiculo '+veiculo.marca+' '+veiculo.modelo+' salvo com sucesso!')
                        .position($scope.getToastPosition())
                        .hideDelay(3000)
                );
            }else{
                $mdToast.show(
                    $mdToast.simple()
                        .content('Problemas ao salvar o Veiculo '+veiculo.marca+' '+veiculo.modelo+' tente novamente se desejar!')
                        .position($scope.getToastPosition())
                        .hideDelay(4000)
                );
            }

        };

        /*Toast do Tipo de Veiculo Alterado*/
        $scope.messageFinalAltera = function(veiculo, t) {
            if(t){
                $mdToast.show(
                    $mdToast.simple()
                        .content('Veiculo '+veiculo.marca+' '+veiculo.modelo+' alterado com sucesso')
                        .position($scope.getToastPosition())
                        .hideDelay(3000)
                );
            }else{
                $mdToast.show(
                    $mdToast.simple()
                        .content('Problema ao alterar o Veiculo '+veiculo.marca+' '+veiculo.modelo+' tente novamente se desejar!')
                        .position($scope.getToastPosition())
                        .hideDelay(4000)
                );
            }

        };

        /*Pega a posicao do toast*/
        $scope.toastPosition = angular.extend({},last);
        $scope.getToastPosition = function() {
            sanitizePosition();
            return Object.keys($scope.toastPosition)
                .filter(function(pos) { return $scope.toastPosition[pos]; })
                .join(' ');
        };
        /*Sanitazi position*/
        function sanitizePosition() {
            var current = $scope.toastPosition;
            if ( current.bottom && last.top ) current.top = false;
            if ( current.top && last.bottom ) current.bottom = false;
            if ( current.right && last.left ) current.left = false;
            if ( current.left && last.right ) current.right = false;
            last = angular.extend({},current);
        }

    })

    /*Controller da sidenav lateral.*/
    .controller('MainCtrl', function ($scope, $timeout, $mdSidenav, $mdUtil, $log) {
        $scope.toggleLeft = buildToggler('left');
        /**
         * Build handler to open/close a SideNav; when animation finishes
         * report completion in console
         */
        function buildToggler(navID) {
            var debounceFn =  $mdUtil.debounce(function(){
                $mdSidenav(navID)
                    .toggle()
                    .then(function () {
                        $log.debug("toggle " + navID + " is done");
                    });
            },200);
            return debounceFn;
        }
    })
    .controller('LeftCtrl', function ($scope, $timeout, $mdSidenav, $log) {
        $scope.closeSideNav = function () {
            $mdSidenav('left').close()
                .then(function () {
                    $log.debug("close LEFT is done");
                });
        };
    });