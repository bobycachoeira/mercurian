/**
 * Created by boby on 19/09/15.
 */

'use strict';
var tipoServicoAlterar;


angular.module('mercurianBobyApp')
    .controller('TipoServicoController', function GetAllTipoServico ($scope, $window, $http, $mdDialog, $mdToast, $timeout, $location) {
        var last = {
            bottom: false,
            top: true,
            left: false,
            right: true
        };

        $http.defaults.useXDomain = true;

      /*  $http.get(servidor+'/cidade').
            success(function (data) {
                $scope.cidades = data;
            });
*/

        $http.get(servidor + '/tiposervico').
            success(function (data) {
                $scope.tiposServicos = data;
            });


        $scope.edit = function (tipoServico) {
            $scope.tipoServico = tipoServico;
            console.log(tipoServico);
            tipoServicoAlterar = tipoServico;
        };

        $scope.novo = function () {
            tipoServicoAlterar = reset();
        };

        //Essa será executada no click do botão edit ("putUser")
        $scope.tipoServico = tipoServicoAlterar;




        $scope.alteraTipoServico = function (tipoServico) {
            $scope.mostraProgressBar = true;
            $http.put(servidor + '/tiposervico/alterar', $scope.tipoServico).success(function () {
                $scope.tiposServicos.unshift();
                $scope.mostraProgressBar = false;
                $scope.messageFinalAltera(tipoServico,true);
                $location.path('/tiposervico');
            }).error(function () {
                $scope.mostraProgressBar = false;
                $scope.messageFinalAltera(tipoServico, false);
                $location.path('/tiposervico/alterar');
            });
        };


        //Essa será executada no click do botão Deletar ("deleteTipoServico")
        $scope.deleteTipoServico = function (tipoServico, event) {

            var url = servidor + '/tiposervico/excluir/' + tipoServico.id;
            $http.delete(url).success(function () {
                $scope.messageFinalDelete(tipoServico, true);
                $location.path('/tiposervico/');
            }).error(function () {
                $scope.messageFinalDelete(tipoServico, false);
                $location.path('/tiposervico/');
            });

        };


        //Essa sera executada quando for clicado o botao Salvar Tipo de servico
        $scope.salvaTipoServico = function (tipoServico) {
            $scope.mostraProgressBar = true;
            $http.post(servidor + '/tiposervico/adicionar', $scope.tipoServico).success(function (data) {
                $scope.tiposServicos.unshift(data);
                $scope.mostraProgressBar = false;
                $scope.messageFinalSalva(tipoServico, true);
                $location.path('/tiposervico');
            }).error(function (data) {
                $scope.mostraProgressBar = false;
                $scope.messageFinalSalva(tipoServico, false);
                $location.path('/tiposervico/incluir');
            });
        };

        //Aqui é a funcao para dar refresh na tela
        /*var refresh = function () {
         $window.location.reload();
         };*/

        var reset = function () {
            $scope.tipoServico = {
                "id": 0,
                "nome": "",
                "descricao": ""
            };
            tipoServicoAlterar = {
                "id": 0,
                "nome": "",
                "descricao": ""
            };
        };

        /*Mensagem de confirma delecao*/
        $scope.ConfirmaDelecao = function(tipoServico, ev) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Deletar o Tipo de Serviço?')
                .content('Tem certeza que deseja deletar o tipo de Serviço '+tipoServico.nome+'?')
                .ariaLabel('Delecao Tipo de Serviço')
                .targetEvent(ev)
                .ok('Sim')
                .cancel('Não');
            $mdDialog.show(confirm).then(function() {
                console.log('Deleta');
                $scope.deleteTipoServico(tipoServico,ev);
            }, function() {
                console.log('Não Deleta');
            });
        };

        /*Toast sera mostrado ao tentar deletar um tipoServico*/
        $scope.messageFinalDelete = function(tipoServico, deletou) {
            if(deletou){
                $mdToast.show(
                    $mdToast.simple()
                        .content('Tipo de Serviço '+tipoServico.nome+' deletado com sucesso')
                        .position($scope.getToastPosition())
                        .hideDelay(3000)
                );
            } else{
                $mdToast.show(
                    $mdToast.simple()
                        .content('Problema ao deletar o Tipo de Serviço '+tipoServico.nome+' tente novamente se desejar')
                        .position($scope.getToastPosition())
                        .hideDelay(4000)
                );
            }

        };

        /*Toast do Tipo de servico salvo*/
        $scope.messageFinalSalva = function(tipoServico, t) {
            if(t){
                $mdToast.show(
                    $mdToast.simple()
                        .content('Tipo de Serviço '+tipoServico.nome+' salvo com sucesso!')
                        .position($scope.getToastPosition())
                        .hideDelay(3000)
                );
            }else{
                $mdToast.show(
                    $mdToast.simple()
                        .content('Problemas ao salvar o Tipo de Serviço '+tipoServico.nome+' tente novamente se desejar!')
                        .position($scope.getToastPosition())
                        .hideDelay(4000)
                );
            }

        };

        /*Toast do Tipo de servico Alterado*/
        $scope.messageFinalAltera = function(tipoServico, t) {
            if(t){
                $mdToast.show(
                    $mdToast.simple()
                        .content('Tipo de Serviço '+tipoServico.nome+' alterado com sucesso')
                        .position($scope.getToastPosition())
                        .hideDelay(3000)
                );
            }else{
                $mdToast.show(
                    $mdToast.simple()
                        .content('Problema ao alterar o Tipo de Serviço '+tipoServico.nome+' tente novamente se desejar!')
                        .position($scope.getToastPosition())
                        .hideDelay(4000)
                );
            }

        };

        /*Pega a posicao do toast*/
        $scope.toastPosition = angular.extend({},last);
        $scope.getToastPosition = function() {
            sanitizePosition();
            return Object.keys($scope.toastPosition)
                .filter(function(pos) { return $scope.toastPosition[pos]; })
                .join(' ');
        };
        /*Sanitazi position*/
        function sanitizePosition() {
            var current = $scope.toastPosition;
            if ( current.bottom && last.top ) current.top = false;
            if ( current.top && last.bottom ) current.bottom = false;
            if ( current.right && last.left ) current.left = false;
            if ( current.left && last.right ) current.right = false;
            last = angular.extend({},current);
        }

    })
