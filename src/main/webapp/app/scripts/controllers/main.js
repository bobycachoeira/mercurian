/**
 * Created by boby on 19/09/15.
 */

'use strict';
var opiniaoAlterar;


angular.module('mercurianBobyApp')
    .controller('MainCtrl', function GetAll($scope, $window, $http, $mdDialog, $mdToast, $timeout, $location) {
        var last = {
            bottom: false,
            top: true,
            left: false,
            right: true
        };

        $http.defaults.useXDomain = true;

        $http.get(servidor + '/cliente').
            success(function (data) {
                $scope.clientes = data;
            });

        $http.get(servidor + '/opiniao').
            success(function (data) {
                $scope.opinioes = data;
            });


        $scope.edit = function (opiniao) {
            $scope.opiniao = opiniao;
            console.log(opiniao);
            opiniaoAlterar = opiniao;
        };

        $scope.novo = function () {
            opiniaoAlterar = reset();
        };

        //Essa será executada no click do botão edit ("putUser")
        $scope.opiniao = opiniaoAlterar;


        $scope.alteraOpiniao = function (opiniao) {
            $http.put(servidor + '/opiniao/alterar', opiniao).success(function () {
                $scope.opinioes.unshift();
                $scope.messageFinalAltera(opiniao, true);
                $location.path('/opiniao');
            }).error(function () {
                $scope.messageFinalAltera(opiniao, false);
                $location.path('/opiniao/alterar');
            });
        };


        //Essa será executada no click do botão Baixar ("baixaOpiniao")
        $scope.baixaOpiniao = function (opiniao, event) {

            $http.put(servidor + '/opiniao/baixar', opiniao).success(function () {
                $scope.messageFinalBaixa(opiniao, true);
                $location.path('/opiniao/');
            }).error(function () {
                $scope.messageFinalBaixa(opiniao, false);
                $location.path('/opiniao/');
            });

        };


        //Essa sera executada quando for clicado o botao Salvar Opiniao
        $scope.salvaOpiniao = function (opiniao) {
            $http.post(servidor + '/opiniao/adicionar', $scope.opiniao).success(function (data) {
                $scope.opinioes.unshift(data);
                $scope.messageFinalSalva(opiniao, true);
                $location.path('/opiniao');
            }).error(function (data) {
                $scope.messageFinalSalva(opiniao, false);
                $location.path('/opiniao/incluir');
            });
        };


        var reset = function () {
            $scope.opiniao = {
                "id": 0,
                "pergunta1": "",
                "pergunta2": "",
                "pergunta3": "",
                "pergunta4": "",
                "pergunta5": null,
                "opniao": "",
                "baixada": false,
                "dataCriacao": "",
                "cliente": {
                    "id": 0,
                    "nome": "",
                    "email": "",
                    "telefone": "",
                    "telefone2": null,
                    "cidade": {
                        "id": 0,
                        "nome": "",
                        "estado": {
                            "id": 0,
                            "nome": "",
                            "uf": ""
                        }
                    }
                }
            };
            opiniaoAlterar = {
                "id": 0,
                "pergunta1": "",
                "pergunta2": "",
                "pergunta3": "",
                "pergunta4": "",
                "pergunta5": null,
                "opniao": "",
                "baixada": false,
                "dataCriacao": "",
                "cliente": {
                    "id": 0,
                    "nome": "",
                    "email": "",
                    "telefone": "",
                    "telefone2": null,
                    "cidade": {
                        "id": 0,
                        "nome": "",
                        "estado": {
                            "id": 0,
                            "nome": "",
                            "uf": ""
                        }
                    }
                }
            };
        };

        /*Mensagem de confirma delecao*/
        $scope.ConfirmaBaixa = function (opiniao, ev) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Baixar Opinião?')
                .content('Tem certeza que deseja dar baixa nesta Opinião do cliente: ' + opiniao.cliente.nome + '?')
                .ariaLabel('Baixa Opiniao')
                .targetEvent(ev)
                .ok('Sim')
                .cancel('Não');
            $mdDialog.show(confirm).then(function () {
                console.log('Baixa');
                $scope.baixaOpiniao(opiniao, ev);
            }, function () {
                console.log('Não Baixa');
            });
        };

        /*Toast sera mostrado ao tentar baixar um opiniao*/
        $scope.messageFinalBaixa = function (opiniao, baixou) {
            if (baixou) {
                $mdToast.show(
                    $mdToast.simple()
                        .content('Opinião baixada com sucesso')
                        .position($scope.getToastPosition())
                        .hideDelay(3000)
                );
            } else {
                $mdToast.show(
                    $mdToast.simple()
                        .content('Problema ao efetuar baixa da Opinião tente novamente se desejar')
                        .position($scope.getToastPosition())
                        .hideDelay(4000)
                );
            }

        };

        /*Toast da opiniao salva*/
        $scope.messageFinalSalva = function (opiniao, t) {
            if (t) {
                $mdToast.show(
                    $mdToast.simple()
                        .content('Opinião salva com sucesso!')
                        .position($scope.getToastPosition())
                        .hideDelay(3000)
                );
            } else {
                $mdToast.show(
                    $mdToast.simple()
                        .content('Problemas ao salvar a Opinião tente novamente se desejar!')
                        .position($scope.getToastPosition())
                        .hideDelay(4000)
                );
            }

        };

        /*Toast da Opiniao Alterado*/
        $scope.messageFinalAltera = function (opiniao, t) {
            if (t) {
                $mdToast.show(
                    $mdToast.simple()
                        .content('Opinião alterada com sucesso')
                        .position($scope.getToastPosition())
                        .hideDelay(3000)
                );
            } else {
                $mdToast.show(
                    $mdToast.simple()
                        .content('Problema ao alterar Opinião tente novamente se desejar!')
                        .position($scope.getToastPosition())
                        .hideDelay(4000)
                );
            }

        };

        /*Pega a posicao do toast*/
        $scope.toastPosition = angular.extend({}, last);
        $scope.getToastPosition = function () {
            sanitizePosition();
            return Object.keys($scope.toastPosition)
                .filter(function (pos) {
                    return $scope.toastPosition[pos];
                })
                .join(' ');
        };
        /*Sanitazi position*/
        function sanitizePosition() {
            var current = $scope.toastPosition;
            if (current.bottom && last.top) current.top = false;
            if (current.top && last.bottom) current.bottom = false;
            if (current.right && last.left) current.left = false;
            if (current.left && last.right) current.right = false;
            last = angular.extend({}, current);
        }

    })
