/**
 * Created by boby on 19/09/15.
 */

'use strict';
var cidadeAlterar;


angular.module('mercurianBobyApp')
    .controller('CidadeController', function GetAllCidade ($scope, $window, $http, $mdDialog, $mdToast, $timeout, $location) {
        var last = {
            bottom: false,
            top: true,
            left: false,
            right: true
        };

        $http.defaults.useXDomain = true;

        //pega todas as cidades
        $http.get(servidor+'/cidade').
            success(function (data) {
                $scope.cidades = data;
            });

        //Pega todos os estados
        $http.get(servidor+'/estado').
            success(function (data) {
                $scope.estados = data;
            });

        $scope.edit = function (cidade) {
            $scope.cidade = cidade;
            console.log(cidade);
            cidadeAlterar = cidade;
        };

        $scope.novo = function () {
            cidadeAlterar = reset();
        };

        //Essa será executada no click do botão edit ("putUser")
        $scope.cidade = cidadeAlterar;




        $scope.alteraCidade = function (cidade) {
            $scope.mostraProgressBar = true;
            $http.put(servidor + '/cidade/alterar', $scope.cidade).success(function () {
                $scope.cidades.unshift();
                $scope.mostraProgressBar = false;
                $scope.messageFinalAltera(cidade,true);
                $location.path('/cidade');
            }).error(function () {
                $scope.mostraProgressBar = false;
                $scope.messageFinalAltera(cidade, false);
                $location.path('/cidade/alterar');
            });
        };


        //Essa será executada no click do botão Deletar ("deleteCidade")
        $scope.deleteCidade = function (cidade, event) {

            var url = servidor + '/cidade/excluir/' + cidade.id;
            $http.delete(url).success(function () {
                $scope.messageFinalDelete(cidade, true);
                $location.path('/cidade/');
            }).error(function () {
                $scope.messageFinalDelete(cidade, false);
                $location.path('/cidade/');
            });

        };


        //Essa sera executada quando for clicado o botao Salvar Cidade
        $scope.salvaCidade = function (cidade) {
            $scope.mostraProgressBar = true;
            $http.post(servidor + '/cidade/adicionar', $scope.cidade).success(function (data) {
                $scope.cidades.unshift(data);
                $scope.mostraProgressBar = false;
                $scope.messageFinalSalva(cidade, true);
                $location.path('/cidade');
            }).error(function (data) {
                $scope.mostraProgressBar = false;
                $scope.messageFinalSalva(cidade, false);
                $location.path('/cidade/incluir');
            });
        };


        var reset = function () {
            $scope.cidade = {
                "id": 0,
                "nome": "",
                "estado": {
                    "id": 0,
                    "nome": "",
                    "uf": ""
                }
            };
            cidadeAlterar = {
                "id": 0,
                "nome": "",
                "estado": {
                    "id": 0,
                    "nome": "",
                    "uf": ""
                }
            };
        };

        /*Mensagem de confirma delecao*/
        $scope.ConfirmaDelecao = function(cidade, ev) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Deletar a Cidade?')
                .content('Tem certeza que deseja deletar a Cidade: '+cidade.nome+'?')
                .ariaLabel('Deleção da Cidade')
                .targetEvent(ev)
                .ok('Sim')
                .cancel('Não');
            $mdDialog.show(confirm).then(function() {
                console.log('Deleta');
                $scope.deleteCidade(cidade,ev);
            }, function() {
                console.log('Não Deleta');
            });
        };

        /*Toast sera mostrado ao tentar deletar uma Cidade*/
        $scope.messageFinalDelete = function(cidade, deletou) {
            if(deletou){
                $mdToast.show(
                    $mdToast.simple()
                        .content('Cidade '+cidade.nome+' deletada com sucesso')
                        .position($scope.getToastPosition())
                        .hideDelay(3000)
                );
            } else{
                $mdToast.show(
                    $mdToast.simple()
                        .content('Problema ao deletar a cidade '+cidade.nome+' tente novamente se desejar')
                        .position($scope.getToastPosition())
                        .hideDelay(4000)
                );
            }

        };

        /*Toast da Cidade salva*/
        $scope.messageFinalSalva = function(cidade, t) {
            if(t){
                $mdToast.show(
                    $mdToast.simple()
                        .content('Cidade '+cidade.nome+' salva com sucesso!')
                        .position($scope.getToastPosition())
                        .hideDelay(3000)
                );
            }else{
                $mdToast.show(
                    $mdToast.simple()
                        .content('Problemas ao salvar a cidade '+cidade.nome+' tente novamente se desejar!')
                        .position($scope.getToastPosition())
                        .hideDelay(4000)
                );
            }

        };

        /*Toast da Cidade Alterada*/
        $scope.messageFinalAltera = function(cidade, t) {
            if(t){
                $mdToast.show(
                    $mdToast.simple()
                        .content('Cidade '+cidade.nome+' alterada com sucesso')
                        .position($scope.getToastPosition())
                        .hideDelay(3000)
                );
            }else{
                $mdToast.show(
                    $mdToast.simple()
                        .content('Problema ao alterar a Cidade '+cidade.nome+' tente novamente se desejar!')
                        .position($scope.getToastPosition())
                        .hideDelay(4000)
                );
            }

        };

        /*Pega a posicao do toast*/
        $scope.toastPosition = angular.extend({},last);
        $scope.getToastPosition = function() {
            sanitizePosition();
            return Object.keys($scope.toastPosition)
                .filter(function(pos) { return $scope.toastPosition[pos]; })
                .join(' ');
        };
        /*Sanitazi position*/
        function sanitizePosition() {
            var current = $scope.toastPosition;
            if ( current.bottom && last.top ) current.top = false;
            if ( current.top && last.bottom ) current.bottom = false;
            if ( current.right && last.left ) current.left = false;
            if ( current.left && last.right ) current.right = false;
            last = angular.extend({},current);
        }

    })
