/**
 * Created by boby on 19/09/15.
 */

'use strict';
var clienteAlterar;


angular.module('mercurianBobyApp')
    .controller('ClienteController', function GetAllClient ($q, $scope, $window, $http, $mdDialog, $mdToast, $timeout, $location) {
        var last = {
            bottom: false,
            top: true,
            left: false,
            right: true
        };

        $http.defaults.useXDomain = true;

        $http.get(servidor+'/cidade').
            success(function (data) {
                $scope.cidades = data;
            });


        $http.get(servidor + '/cliente').
            success(function (data) {
                $scope.clientes = data;
            });


        $scope.edit = function (cliente) {
            $scope.cliente = cliente;
            console.log(cliente);
            clienteAlterar = cliente;
        };

        $scope.novo = function () {
            clienteAlterar = reset();
        };

        //Essa será executada no click do botão edit ("putUser")
        $scope.cliente = clienteAlterar;




        $scope.alteraCliente = function (cliente) {
            $scope.mostraProgressBar = true;
            $http.put(servidor + '/cliente/alterar', $scope.cliente).success(function () {
                $scope.clientes.unshift();
                $scope.mostraProgressBar = false;
                $scope.messageFinalAltera(cliente,true);
                $location.path('/clientes');
            }).error(function () {
                $scope.mostraProgressBar = false;
                $scope.messageFinalAltera(cliente, false);
                $location.path('/clientes/alterar');
            });
        };


        //Essa será executada no click do botão Deletar ("deleteCliente")
        $scope.deleteCliente = function (cliente, event) {

                var url = servidor + '/cliente/excluir/' + cliente.id;
            $http.delete(url).success(function () {
                    $scope.messageFinalDelete(cliente, true);
                    $location.path('/clientes/');
                    /*$window.confirm('Cliente: '+cliente.nome+' deletado com sucesso!')
                    $window.location.reload();*/
                }).error(function (errorResponse) {

                    $scope.messageFinalDelete(cliente, false);
                    $location.path('/clientes/');
                });

        };


        //Essa sera executada quando for clicado o botao Salvar Cliente
        $scope.salvaCliente = function (cliente) {
            $scope.mostraProgressBar = true;
            $http.post(servidor + '/cliente/adicionar', $scope.cliente).success(function (data) {
                $scope.clientes.unshift(data);
                $scope.mostraProgressBar = false;
                $scope.messageFinalSalva(cliente, true);
                $location.path('/clientes');
            }).error(function (data) {
                $scope.mostraProgressBar = false;
                $scope.messageFinalSalva(cliente, false, data);
                $location.path('/clientes/incluir');
            });
        };

        //Aqui é a funcao para dar refresh na tela
        /*var refresh = function () {
         $window.location.reload();
         };*/

        var reset = function () {
            $scope.cliente = {
                id: "", nome: "", email: "", telefone: "", telefone2: "","cidade": {
                    "id": "",
                    "nome": "",
                    "estado": {
                        "id": "",
                        "nome": "",
                        "uf": ""
                    }
                }
            };
            clienteAlterar = {
                id: 0, nome: "", email: "", telefone: "", telefone2: "", "cidade": {
                    "id": 0,
                    "nome": "",
                    "estado": {
                        "id": 0,
                        "nome": "",
                        "uf": ""
                    }
                }
            };
        };

        /*Mensagem de confirma delecao*/
        $scope.ConfirmaDelecao = function(cliente, ev) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Deletar o cliente?')
                .content('Tem certeza que deseja deletar o cliente '+cliente.nome+'?')
                .ariaLabel('Deleção cliente')
                .targetEvent(ev)
                .ok('Sim')
                .cancel('Não');
            $mdDialog.show(confirm).then(function() {
                console.log('Deleta');
                $scope.deleteCliente(cliente,ev);
            }, function() {
                console.log('Não Deleta');
            });
        };

        /*Toast sera mostrado ao tentar deletar um cliente*/
        $scope.messageFinalDelete = function(cliente, deletou) {
            if(deletou){
                $mdToast.show(
                    $mdToast.simple()
                        .content('Cliente '+cliente.nome+' deletado com sucesso')
                        .position($scope.getToastPosition())
                        .hideDelay(3000)
                );
            } else{
                $mdToast.show(
                    $mdToast.simple()
                        .content('Problema ao deletar o cliente '+cliente.nome+' tente novamente se desejar')
                        .position($scope.getToastPosition())
                        .hideDelay(4000)
                );
            }

        };

        /*Toast do cliente salvo*/
        $scope.messageFinalSalva = function(cliente, t, data) {
            if(t){
                $mdToast.show(
                    $mdToast.simple()
                        .content('Cliente '+cliente.nome+' salvo com sucesso!')
                        .position($scope.getToastPosition())
                        .hideDelay(3000)
                );
            }else{
                $mdToast.show(
                    $mdToast.simple()
                        .content('Problemas ao salvar o cliente '+cliente.nome+' tente novamente se desejar!')
                        .position($scope.getToastPosition())
                        .hideDelay(4000)
                );
                console.log(data);
            }

        };

        /*Toast do cliente Alterado*/
        $scope.messageFinalAltera = function(cliente, t) {
            if(t){
                $mdToast.show(
                    $mdToast.simple()
                        .content('Cliente '+cliente.nome+' alterado com sucesso')
                        .position($scope.getToastPosition())
                        .hideDelay(3000)
                );
            }else{
                $mdToast.show(
                    $mdToast.simple()
                        .content('Problema ao alterar o cliente '+cliente.nome+' tente novamente se desejar!')
                        .position($scope.getToastPosition())
                        .hideDelay(4000)
                );
            }

        };

        /*Pega a posicao do toast*/
        $scope.toastPosition = angular.extend({},last);
        $scope.getToastPosition = function() {
            sanitizePosition();
            return Object.keys($scope.toastPosition)
                .filter(function(pos) { return $scope.toastPosition[pos]; })
                .join(' ');
        };
        /*Sanitazi position*/
        function sanitizePosition() {
            var current = $scope.toastPosition;
            if ( current.bottom && last.top ) current.top = false;
            if ( current.top && last.bottom ) current.bottom = false;
            if ( current.right && last.left ) current.left = false;
            if ( current.left && last.right ) current.right = false;
            last = angular.extend({},current);
        }

    })

    /*Controller da sidenav lateral.*/
 /*   .controller('MainCtrl', function ($scope, $timeout, $mdSidenav, $mdUtil, $log) {
        $scope.toggleLeft = buildToggler('left');
        /!**
         * Build handler to open/close a SideNav; when animation finishes
         * report completion in console
         *!/
        function buildToggler(navID) {
            var debounceFn =  $mdUtil.debounce(function(){
                $mdSidenav(navID)
                    .toggle()
                    .then(function () {
                        $log.debug("toggle " + navID + " is done");
                    });
            },200);
            return debounceFn;
        }
    })
    .controller('LeftCtrl', function ($scope, $timeout, $mdSidenav, $log) {
        $scope.closeSideNav = function () {
            $mdSidenav('left').close()
                .then(function () {
                    $log.debug("close LEFT is done");
                });
        };
    });*/