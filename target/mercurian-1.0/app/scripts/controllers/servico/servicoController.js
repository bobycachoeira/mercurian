/**
 * Created by boby on 19/09/15.
 */

'use strict';
var servicoAlterar;


angular.module('mercurianBobyApp')
    .controller('ServicoController', function GetAllServico($scope, $window, $http, $mdDialog, $mdToast, $timeout, $location) {
        var last = {
            bottom: false,
            top: true,
            left: false,
            right: true
        };

        $http.defaults.useXDomain = true;

        //pega a lista de servicos
        $http.get(servidor + '/servico').
            success(function (data) {
                $scope.servicos = data;
            });

        //pega a lista de tipos de servico
        $http.get(servidor + '/tiposervico').
            success(function (data) {
                $scope.tipoServicos = data;
            });

        //pega a lista de clientes
        $http.get(servidor + '/cliente').
            success(function (data) {
                $scope.clientes = data;
            });


        //pega a lista de veiculos
        /*$http.get(servidor + '/veiculo').
         success(function (data) {
         $scope.veiculos = data;
         });*/

        $scope.carregaVeiculos = function (id) {
            $http.get(servidor + '/veiculo/pesquisarporcliente/'+id).
                success(function (data) {
                    $scope.veiculos = data;
                });
        };


            $scope.edit = function (servico) {
                $scope.servico = servico;
                console.log(servico);
                servicoAlterar = servico;
            };

            $scope.novo = function () {
                servicoAlterar = reset();
            };

            //Essa será executada no click do botão edit ("putUser")
            $scope.servico = servicoAlterar;


            $scope.alteraServico = function (servico) {
                $scope.mostraProgressBar = true;
                $http.put(servidor + '/servico/alterar', $scope.servico).success(function () {
                    $scope.servicos.unshift();
                    $scope.mostraProgressBar = false;
                    $scope.messageFinalAltera(servico, true);
                    $location.path('/servico');
                }).error(function () {
                    $scope.mostraProgressBar = false;
                    $scope.messageFinalAltera(servico, false);
                    $location.path('/servico/alterar');
                });
            };


            //Essa será executada no click do botão Deletar ("deleteServico")
            $scope.deleteServico = function (servico, event) {

                var url = servidor + '/servico/excluir/' + servico.id;
                $http.delete(url).success(function () {
                    $scope.messageFinalDelete(servico, true);
                    $location.path('/servico/');
                }).error(function () {
                    $scope.messageFinalDelete(servico, false);
                    $location.path('/servico/');
                });

            };


            //Essa sera executada quando for clicado o botao Salvar o servico
            $scope.salvaServico = function (servico) {
                $scope.mostraProgressBar = true;
                $scope.messageFinalSalvando(servico);
                $http.post(servidor + '/servico/adicionar', $scope.servico).success(function (data) {
                    $scope.servicos.unshift(data);
                    $scope.mostraProgressBar = false;
                    $scope.messageFinalSalva(servico, true);
                    $location.path('/servico');
                }).error(function (data) {
                    $scope.mostraProgressBar = false;
                    $scope.messageFinalSalva(servico, false);
                    $location.path('/servico/incluir');
                });
            };


            var reset = function () {
                $scope.servico = {
                    "id": 0,
                    "observacao": "",
                    "cliente": {
                        "id": 0,
                        "nome": "",
                        "email": "",
                        "telefone": "",
                        "telefone2": "",
                        "cidade": {
                            "id": 0,
                            "nome": "",
                            "estado": {
                                "id": 0,
                                "nome": "",
                                "uf": ""
                            }
                        }
                    },
                    "veiculo": {
                        "id": 1,
                        "marca": "",
                        "modelo": "",
                        "ano": 0,
                        "placa": "",
                        "cliente": {
                            "id": 0,
                            "nome": "",
                            "email": "",
                            "telefone": "",
                            "telefone2": "",
                            "cidade": {
                                "id": 0,
                                "nome": "",
                                "estado": {
                                    "id": 0,
                                    "nome": "",
                                    "uf": ""
                                }
                            }
                        }
                    },
                    "servico": {
                        "id": 0,
                        "nome": "",
                        "descricao": ""
                    }
                };
                servicoAlterar = {
                    "id": 0,
                    "observacao": "",
                    "cliente": {
                        "id": 0,
                        "nome": "",
                        "email": "",
                        "telefone": "",
                        "telefone2": "",
                        "cidade": {
                            "id": 0,
                            "nome": "",
                            "estado": {
                                "id": 0,
                                "nome": "",
                                "uf": ""
                            }
                        }
                    },
                    "veiculo": {
                        "id": 1,
                        "marca": "",
                        "modelo": "",
                        "ano": 0,
                        "placa": "",
                        "cliente": {
                            "id": 0,
                            "nome": "",
                            "email": "",
                            "telefone": "",
                            "telefone2": "",
                            "cidade": {
                                "id": 0,
                                "nome": "",
                                "estado": {
                                    "id": 0,
                                    "nome": "",
                                    "uf": ""
                                }
                            }
                        }
                    },
                    "servico": {
                        "id": 0,
                        "nome": "",
                        "descricao": ""
                    }
                };
            };

            /*Mensagem de confirma delecao*/
            $scope.ConfirmaDelecao = function (servico, ev) {
                // Appending dialog to document.body to cover sidenav in docs app
                var confirm = $mdDialog.confirm()
                    .title('Deletar o serviço?')
                    .content('Tem certeza que deseja deletar o serviço ' + servico.observacao + '| para o cliente: ' + servico.cliente.nome)
                    .ariaLabel('Delecao serviço')
                    .targetEvent(ev)
                    .ok('Sim')
                    .cancel('Não');
                $mdDialog.show(confirm).then(function () {
                    console.log('Deleta');
                    $scope.deleteServico(servico, ev);
                }, function () {
                    console.log('Não Deleta');
                });
            };

            /*Toast sera mostrado ao tentar deletar um Servico*/
            $scope.messageFinalDelete = function (servico, deletou) {
                if (deletou) {
                    $mdToast.show(
                        $mdToast.simple()
                            .content('Serviço ' + servico.observacao + ' deletado com sucesso')
                            .position($scope.getToastPosition())
                            .hideDelay(3000)
                    );
                } else {
                    $mdToast.show(
                        $mdToast.simple()
                            .content('Problema ao deletar o Serviço ' + servico.observacao + ' tente novamente se desejar')
                            .position($scope.getToastPosition())
                            .hideDelay(4000)
                    );
                }

            };

            /*Toast do Tipo de Servico salvo*/
            $scope.messageFinalSalva = function (servico, t) {
                if (t) {
                    $mdToast.show(
                        $mdToast.simple()
                            .content('Serviço ' + servico.observacao + ' salvo com sucesso!')
                            .position($scope.getToastPosition())
                            .hideDelay(3000)
                    );
                } else {
                    $mdToast.show(
                        $mdToast.simple()
                            .content('Problemas ao salvar o Serviço ' + servico.observacao + ' tente novamente se desejar!')
                            .position($scope.getToastPosition())
                            .hideDelay(4000)
                    );
                }

            };

            /*Toast do Tipo de Servico Alterado*/
            $scope.messageFinalAltera = function (servico, t) {
                if (t) {
                    $mdToast.show(
                        $mdToast.simple()
                            .content('Serviço ' + servico.observacao + ' alterado com sucesso')
                            .position($scope.getToastPosition())
                            .hideDelay(3000)
                    );
                } else {
                    $mdToast.show(
                        $mdToast.simple()
                            .content('Problema ao alterar o Serviço ' + servico.observacao + ' tente novamente se desejar!')
                            .position($scope.getToastPosition())
                            .hideDelay(4000)
                    );
                }

            };

        /*Toast de mensagem salvando servico Servico Alterado*/
        $scope.messageFinalSalvando = function (servico) {
                  $mdToast.show(
                    $mdToast.simple()
                        .content('Aguarde que estamos salvando o Serviço ' + servico.observacao + ' e enviando email ao cliente')
                        .position($scope.getToastPosition())
                        .hideDelay(20000)
                );
        };

            /*Pega a posicao do toast*/
            $scope.toastPosition = angular.extend({}, last);
            $scope.getToastPosition = function () {
                sanitizePosition();
                return Object.keys($scope.toastPosition)
                    .filter(function (pos) {
                        return $scope.toastPosition[pos];
                    })
                    .join(' ');
            };
            /*Sanitazi position*/
            function sanitizePosition() {
                var current = $scope.toastPosition;
                if (current.bottom && last.top) current.top = false;
                if (current.top && last.bottom) current.bottom = false;
                if (current.right && last.left) current.left = false;
                if (current.left && last.right) current.right = false;
                last = angular.extend({}, current);
            }

        }
        )
