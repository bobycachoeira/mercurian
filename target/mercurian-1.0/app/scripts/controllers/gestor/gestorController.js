/**
 * Created by boby on 19/09/15.
 */

'use strict';
var gestorAlterar;


angular.module('mercurianBobyApp')
    .controller('GestorController', function GetAllGestores ($scope, $window, $http, $mdDialog, $mdToast, $timeout, $location) {
        var last = {
            bottom: false,
            top: true,
            left: false,
            right: true
        };

        $http.defaults.useXDomain = true;


        $http.get(servidor + '/gestor').
            success(function (data) {
                $scope.gestores = data;
            });


        $scope.edit = function (gestor) {
            $scope.gestor = gestor;
            console.log(gestor);
            gestorAlterar = gestor;
        };

        $scope.novo = function () {
            gestorAlterar = reset();
        };

        //Essa será executada no click do botão edit ("putUser")
        $scope.gestor = gestorAlterar;




        $scope.alteraGestor = function (gestor) {
            $scope.mostraProgressBar = true;
            $http.put(servidor + '/gestor/alterar', $scope.gestor).success(function () {
                $scope.gestores.unshift();
                $scope.mostraProgressBar = false;
                $scope.messageFinalAltera(gestor,true);
                $location.path('/gestor');
            }).error(function () {
                $scope.mostraProgressBar = false;
                $scope.messageFinalAltera(gestor, false);
                $location.path('/gestor/alterar');
            });
        };


        //Essa será executada no click do botão Deletar ("deleteGestor")
        $scope.deleteGestor = function (gestor, event) {

            var url = servidor + '/gestor/excluir/' + gestor.id;
            $http.delete(url).success(function () {
                $scope.messageFinalDelete(gestor, true);
                $location.path('/gestor/');
            }).error(function () {
                $scope.messageFinalDelete(gestor, false);
                $location.path('/gestor/');
            });

        };


        //Essa sera executada quando for clicado o botao Salvar Tipo de Gestor
        $scope.salvaGestor = function (gestor) {
            $scope.mostraProgressBar = true;
            $http.post(servidor + '/gestor/adicionar', $scope.gestor).success(function (data) {
                $scope.gestores.unshift(data);
                $scope.mostraProgressBar = false;
                $scope.messageFinalSalva(gestor, true);
                $location.path('/gestor');
            }).error(function (data) {
                $scope.mostraProgressBar = false;
                $scope.messageFinalSalva(gestor, false);
                $location.path('/gestor/incluir');
            });
        };

        var reset = function () {
            $scope.gestor = {
                "id": 0,
                "nome": "",
                "email": ""
            };
            gestorAlterar = {
                "id": 0,
                "nome": "",
                "email": ""
            };
        };

        /*Mensagem de confirma delecao*/
        $scope.ConfirmaDelecao = function(gestor, ev) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Deletar Gestor?')
                .content('Tem certeza que deseja deletar o gestor '+gestor.nome+'?')
                .ariaLabel('Deleção Gestor')
                .targetEvent(ev)
                .ok('Sim')
                .cancel('Não');
            $mdDialog.show(confirm).then(function() {
                console.log('Deleta');
                $scope.deleteGestor(gestor,ev);
            }, function() {
                console.log('Não Deleta');
            });
        };

        /*Toast sera mostrado ao tentar deletar um Gestor*/
        $scope.messageFinalDelete = function(gestor, deletou) {
            if(deletou){
                $mdToast.show(
                    $mdToast.simple()
                        .content('Gestor '+gestor.nome+' deletado com sucesso')
                        .position($scope.getToastPosition())
                        .hideDelay(3000)
                );
            } else{
                $mdToast.show(
                    $mdToast.simple()
                        .content('Problema ao deletar o Gestor '+gestor.nome+' tente novamente se desejar')
                        .position($scope.getToastPosition())
                        .hideDelay(4000)
                );
            }

        };

        /*Toast do Tipo de Gestor salvo*/
        $scope.messageFinalSalva = function(gestor, t) {
            if(t){
                $mdToast.show(
                    $mdToast.simple()
                        .content('Gestor '+gestor.nome+' salvo com sucesso!')
                        .position($scope.getToastPosition())
                        .hideDelay(3000)
                );
            }else{
                $mdToast.show(
                    $mdToast.simple()
                        .content('Problemas ao salvar o Gestor '+gestor.nome+' tente novamente se desejar!')
                        .position($scope.getToastPosition())
                        .hideDelay(4000)
                );
            }

        };

        /*Toast do Tipo de Gestor Alterado*/
        $scope.messageFinalAltera = function(gestor, t) {
            if(t){
                $mdToast.show(
                    $mdToast.simple()
                        .content('Gestor '+gestor.nome+' alterado com sucesso')
                        .position($scope.getToastPosition())
                        .hideDelay(3000)
                );
            }else{
                $mdToast.show(
                    $mdToast.simple()
                        .content('Problema ao alterar o Gestor '+gestor.nome+' tente novamente se desejar!')
                        .position($scope.getToastPosition())
                        .hideDelay(4000)
                );
            }

        };

        /*Pega a posicao do toast*/
        $scope.toastPosition = angular.extend({},last);
        $scope.getToastPosition = function() {
            sanitizePosition();
            return Object.keys($scope.toastPosition)
                .filter(function(pos) { return $scope.toastPosition[pos]; })
                .join(' ');
        };
        /*Sanitazi position*/
        function sanitizePosition() {
            var current = $scope.toastPosition;
            if ( current.bottom && last.top ) current.top = false;
            if ( current.top && last.bottom ) current.bottom = false;
            if ( current.right && last.left ) current.left = false;
            if ( current.left && last.right ) current.right = false;
            last = angular.extend({},current);
        }

    })
