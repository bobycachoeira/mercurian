/**
 * Created by boby on 19/09/15.
 */

'use strict';
var opiniaoAlterar;
var apareceBotao = true;

google.setOnLoadCallback(function () {
    angular.bootstrap(document.body, ['my-app']);
});
google.load('visualization', '1', {packages: ['corechart']});



angular.module('mercurianBobyApp')
    .controller('InicioController', function GetAllOpiniao($scope, $window, $http, $mdDialog, $mdToast, $timeout, $location) {



        $http.get(servidor + '/pergunta1').
            success(function (data) {
                $scope.pergunta1 = data;
                $scope.otimoPergunta1 = $scope.pergunta1.otimo;
                $scope.bomPergunta1 = $scope.pergunta1.bom;
                $scope.regularPergunta1 = $scope.pergunta1.regular;
                $scope.ruimPergunta1 = $scope.pergunta1.ruim;
                $scope.pessimoPergunta1 = $scope.pergunta1.pessimo;
                $scope.numeroDeOpinoes = $scope.pergunta1.numeroOpinioes;

                //Monta o terceiro Grafico
                var data1 = new google.visualization.DataTable();
                data1.addColumn('string', 'Nome');
                data1.addColumn('number', 'Qty');
                data1.addRow(["Otimo",$scope.otimoPergunta1]);
                data1.addRow(["Bom",$scope.bomPergunta1]);
                data1.addRow(["Regular",$scope.regularPergunta1]);
                data1.addRow(["Ruim",$scope.ruimPergunta1]);
                data1.addRow(["Pessimo",$scope.pessimoPergunta1]);
                var options = {
                    title: "Opinião dos clientes sobre o serviço prestado || De um total de: "+$scope.numeroDeOpinoes+" Opiniões"
                };

                var chart = new google.visualization.PieChart(document.getElementById('grafico1'));
                chart.draw(data1, options);
            });

        $http.get(servidor + '/pergunta2').
            success(function (data) {
                $scope.pergunta2 = data;
                $scope.simPergunta2 = $scope.pergunta2.sim;
                $scope.naoPergunta2 = $scope.pergunta2.nao;
                $scope.talvezPergunta2 = $scope.pergunta2.talvez;
                $scope.numeroDeOpinoes = $scope.pergunta2.numeroOpinioes;

                //Monta o terceiro Grafico
                var data2 = new google.visualization.DataTable();
                data2.addColumn('string', 'Nome');
                data2.addColumn('number', 'Qty');
                data2.addRow(["Sim",$scope.simPergunta2]);
                data2.addRow(["Não",$scope.naoPergunta2]);
                data2.addRow(["Talvez",$scope.talvezPergunta2]);
                var options = {
                    title: "Clientes Voltariam ou nao a fazer serviços conosco? || De um total de: "+$scope.numeroDeOpinoes+" Opiniões"
                };

                var chart = new google.visualization.PieChart(document.getElementById('grafico2'));
                chart.draw(data2, options);
            });


        $http.get(servidor + '/pergunta3').
            success(function (data) {
                $scope.pergunta3 = data;
                $scope.otimoPergunta3 = $scope.pergunta3.otimo;
                $scope.bomPergunta3 = $scope.pergunta3.bom;
                $scope.regularPergunta3 = $scope.pergunta3.regular;
                $scope.ruimPergunta3 = $scope.pergunta3.ruim;
                $scope.pessimoPergunta3 = $scope.pergunta3.pessimo;
                $scope.numeroDeOpinoes = $scope.pergunta3.numeroOpinioes;

                //Monta o terceiro Grafico
                var data3 = new google.visualization.DataTable();
                data3.addColumn('string', 'Nome');
                data3.addColumn('number', 'Qty');
                data3.addRow(["Otimo",$scope.otimoPergunta3]);
                data3.addRow(["Bom",$scope.bomPergunta3]);
                data3.addRow(["Regular",$scope.regularPergunta3]);
                data3.addRow(["Ruim",$scope.ruimPergunta3]);
                data3.addRow(["Pessimo",$scope.pessimoPergunta3]);
                var options = {
                    title: "Opinião dos clientes a respeito do atendimento na loja || De um total de: "+$scope.numeroDeOpinoes+" Opiniões"
                };

                var chart = new google.visualization.PieChart(document.getElementById('grafico3'));
                chart.draw(data3, options);

            });

        var last = {
            bottom: false,
            top: true,
            left: false,
            right: true
        };

        $http.defaults.useXDomain = true;

        /*$http.get(servidor + '/cliente').
            success(function (data) {
                $scope.clientes = data;
            });*/

        $http.get(servidor + '/opiniao').
            success(function (data) {
                $scope.opinioes = data;
            });


        //Essa será executada no click do botão Baixar ("baixaOpiniao")
        $scope.baixaOpiniao = function (opiniao, event) {
            $scope.mostraProgressBar = true;
            $http.put(servidor + '/opiniao/baixar' , opiniao).success(function () {
                $scope.mostraProgressBar = false;
                $scope.messageFinalBaixa(opiniao, true);
                $location.path('/inicio/');
            }).error(function () {
                $scope.mostraProgressBar = false;
                $scope.messageFinalBaixa(opiniao, false);
                $location.path('/inicio/');
            });

        };



        $scope.edit = function (opiniao) {
            $scope.opiniao = opiniao;
            console.log(opiniao);
            opiniaoAlterar = opiniao;
        };


        var reset = function () {
            $scope.opiniao = {
                "id": 0,
                "pergunta1": "",
                "pergunta2": "",
                "pergunta3": "",
                "pergunta4": "",
                "pergunta5": null,
                "opniao": "",
                "baixada": false,
                "dataCriacao": "",
                "cliente": {
                    "id": 0,
                    "nome": "",
                    "email": "",
                    "telefone": "",
                    "telefone2": null,
                    "cidade": {
                        "id": 0,
                        "nome": "",
                        "estado": {
                            "id": 0,
                            "nome": "",
                            "uf": ""
                        }
                    }
                }
            };
        };

        /*Mensagem de confirma baixa*/
        $scope.ConfirmaBaixa = function (opiniao, ev) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Baixar Opinião?')
                .content('Tem certeza que deseja dar baixa nesta Opinião do cliente: ' + opiniao.cliente.nome + '?')
                .ariaLabel('Baixa Opinião')
                .targetEvent(ev)
                .ok('Sim')
                .cancel('Não');
            $mdDialog.show(confirm).then(function () {
                console.log('Baixa');
                $scope.baixaOpiniao(opiniao, ev);
            }, function () {
                console.log('Não Baixa');
            });
        };

        /*Toast sera mostrado ao tentar baixar um opiniao*/
        $scope.messageFinalBaixa = function (opiniao, baixou) {
            if (baixou) {
                $mdToast.show(
                    $mdToast.simple()
                        .content('Opinião baixada com sucesso')
                        .position($scope.getToastPosition())
                        .hideDelay(3000)
                );
            } else {
                $mdToast.show(
                    $mdToast.simple()
                        .content('Problema ao efetuar baixa da Opinião tente novamente se desejar')
                        .position($scope.getToastPosition())
                        .hideDelay(4000)
                );
            }

        };

        /*Pega a posicao do toast*/
        $scope.toastPosition = angular.extend({}, last);
        $scope.getToastPosition = function () {
            sanitizePosition();
            return Object.keys($scope.toastPosition)
                .filter(function (pos) {
                    return $scope.toastPosition[pos];
                })
                .join(' ');
        };
        /*Sanitazi position*/
        function sanitizePosition() {
            var current = $scope.toastPosition;
            if (current.bottom && last.top) current.top = false;
            if (current.top && last.bottom) current.bottom = false;
            if (current.right && last.left) current.left = false;
            if (current.left && last.right) current.right = false;
            last = angular.extend({}, current);
        }


    });
