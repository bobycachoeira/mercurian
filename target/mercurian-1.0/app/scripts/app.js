'use strict';
var servidor = "http://localhost:8080/mercurianservices-0.0.1-SNAPSHOT/api";
//var servidor = "http://mercurianservices-bobycachoeira.rhcloud.com/api";

var mostraMenu = false;
/**
 * @ngdoc overview
 * @name mercurianBobyApp
 * @description
 * # mercurianBobyApp
 *
 * Main module of the application.
 */
angular
  .module('mercurianBobyApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngMaterial'
  ])
  .config(function ($routeProvider, $httpProvider) {
    $routeProvider
      .when('/inicio', {
        templateUrl: 'views/main.html',
        controller: 'InicioController'
      })
      .when('/login', {
        templateUrl: 'views/login/login.html',
        controller: 'LoginController',
      })

      //Aqui esta o roteamento do CRUD do cliente.
        .when('/clientes', {
          templateUrl: 'views/cliente/listaCliente.html',
          controller: 'ClienteController'
        })

        .when('/clientes/alterar', {
          templateUrl: 'views/cliente/alteraCliente.html',
          controller: 'ClienteController'
        })

          .when('/clientes/incluir', {
            templateUrl: 'views/cliente/incluirCliente.html',
            controller: 'ClienteController'
          })

      //Aqui esta o roteamento do CRUD do Tipo de servico.
        .when('/tiposervico', {
          templateUrl: 'views/tipoServico/listaTipoServico.html',
          controller: 'TipoServicoController'
        })

        .when('/tiposervico/alterar', {
          templateUrl: 'views/tipoServico/alteraTipoServico.html',
          controller: 'TipoServicoController'
        })

        .when('/tiposervico/incluir', {
          templateUrl: 'views/tipoServico/incluirTipoServico.html',
          controller: 'TipoServicoController'
        })

      //Aqui esta o roteamento do CRUD do Veiculo.
        .when('/veiculo', {
          templateUrl: 'views/veiculo/listaVeiculo.html',
          controller: 'VeiculoController'
        })

        .when('/veiculo/alterar', {
          templateUrl: 'views/veiculo/alteraVeiculo.html',
          controller: 'VeiculoController'
        })

        .when('/veiculo/incluir', {
          templateUrl: 'views/veiculo/incluirVeiculo.html',
          controller: 'VeiculoController'
        })

      //Aqui esta o roteamento do CRUD do Servico.
        .when('/servico', {
          templateUrl: 'views/servico/listaServico.html',
          controller: 'ServicoController'
        })

        .when('/servico/alterar', {
          templateUrl: 'views/servico/alteraServico.html',
          controller: 'ServicoController'
        })

        .when('/servico/incluir', {
          templateUrl: 'views/servico/incluirServico.html',
          controller: 'ServicoController'
        })

        //Aqui esta o roteamento do CRUD da Cidade.
        .when('/cidade', {
            templateUrl: 'views/cidade/listaCidade.html',
            controller: 'CidadeController'
        })

        .when('/cidade/alterar', {
            templateUrl: 'views/cidade/alteraCidade.html',
            controller: 'CidadeController'
        })

        .when('/cidade/incluir', {
            templateUrl: 'views/cidade/incluirCidade.html',
            controller: 'CidadeController'
        })

        //Aqui esta o roteamento do CRUD do Gestor.
        .when('/gestor', {
            templateUrl: 'views/gestor/listaGestor.html',
            controller: 'GestorController'
        })

        .when('/gestor/alterar', {
            templateUrl: 'views/gestor/alteraGestor.html',
            controller: 'GestorController'
        })

        .when('/gestor/incluir', {
            templateUrl: 'views/gestor/incluirGestor.html',
            controller: 'GestorController'
        })

        //Aqui esta o roteamento do CRUD do Gestor.
        .when('/opiniao', {
            templateUrl: 'views/opiniao/listaOpiniao.html',
            controller: 'OpiniaoController'
        })

        .when('/opiniao/alterar', {
            templateUrl: 'views/opiniao/alteraOpiniao.html',
            controller: 'OpiniaoController'
        })

        .when('/opiniao/incluir', {
            templateUrl: 'views/opiniao/incluirOpiniao.html',
            controller: 'OpiniaoController'
        })

        .when('/opiniao/visualiza', {
          templateUrl: 'views/opiniao/visualizaOpiniao.html',
          controller: 'OpiniaoController'
        })

        .otherwise({
        redirectTo: '/'
      });
  });
